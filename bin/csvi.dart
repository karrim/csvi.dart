#!/usr/bin/env dart
/// Copyright (C) 2015  Andrea Cantafio
///
/// This program is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License along
/// with this program; if not, write to the Free Software Foundation, Inc.,
/// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import 'dart:async';
import 'dart:core' hide print;
import 'dart:io';

import 'package:args/args.dart';
import 'package:args/command_runner.dart';
import 'package:console/console.dart';
import 'package:csvi/csvi.dart';
import 'package:postgresql/pool.dart';
import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';

main(List<String> args) async {
  Console.init();
  Console.setBackgroundColor(defaultBackgroundColor.id);
  Console.setTextColor(defaultTextColor.id);

  final runner = new CommandRunner('csvi', 'Command line csv manipulator.')
    ..addCommand(new ConvertCommand())
    ..addCommand(new RenameCommand())
    ..addCommand(new RmColCommand());
  //try {
    await runner.run(args);
  /*} catch(e) {
    print(e, color: Color.RED);
  }*/
}
