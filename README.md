#csvi.dart
Command line csv manipulator.

##Requirements
* [Dart sdk](https://www.dartlang.org/).

##Installation
Add  
```
export PATH=$PATH:~/.pub-cache/bin
```  
to  ~/.bashrc.  

Run  
```
pub global activate -sgit https://bitbucket.org/karrim/csvi.dart.git
```

##Usage
```
csvi [command] [params]
```  
Available commands:

* ```convert``` (now supports only csv -> sql)
* ```help```
* ```ren```
* ```rm```

Some commands require to enter characters like `\n`, `\r`.  
To enter correctly these characters into shell you need to wrap them with `$'<char>'`:
`$'\n'`, `$'\r'`.