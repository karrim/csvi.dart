/// Copyright (C) 2015  Andrea Cantafio
///
/// This program is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License along
/// with this program; if not, write to the Free Software Foundation, Inc.,
/// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
library csvi.src.command;

import 'dart:core' hide print;
import 'dart:io' show File;

import 'package:args/command_runner.dart';
import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';
import 'package:csvi/csvi.dart';
import 'package:path/path.dart';

class _SqlInfo {
  _SqlInfo();

  int length = -1;
  bool nullable = false;
  int precision = -1;
  String type;
}

class ConvertCommand extends Command {
  ConvertCommand() {
    addIoOptions(argParser);
    addParsingOptions(argParser);
  }

  @override
  void run() {
    final i = argResults['in'];
    if (null == i) {
      throw new ArgumentError.notNull('in');
    }
    final o = argResults['out'];
    if (null == o) {
      throw new ArgumentError.notNull('out');
    }
    final e = argResults['end-of-line'];
    final f = argResults['field-delimiter'];
    final t = argResults['text-delimiter'];
    final csv = new File(i).readAsStringSync();
    final detector = new FirstOccurenceSettingsDetector(
        eols: e, fieldDelimiters: f, textDelimiters: t);
    final decoded = decodeToList(csv, detector: detector);
    final fields = decoded[0];
    final types =
        new List<_SqlInfo>.generate(fields.length, (_) => new _SqlInfo());
    switch (extension(o)) {
      case '.sql':
        for (int i = 0; i < fields.length; ++i) {
          var len = -1, nullable = false, numeric = true, precision = -1;
          for (int j = 1; j < decoded.length; ++j) {
            var l;
            final value = decoded[j][i];
            if (numeric) {
              var n = num.parse(value, (_) => double.INFINITY);
              if (double.INFINITY == n) {
                numeric = false;
                l = value.toString().length;
              } else {
                if (n is int) {
                  l = n.toString().length;
                  if (-1 != precision) {
                    l += precision;
                  }
                } else if (n is double) {
                  var split = n.toString().split('.');
                  l = split[0].length;
                  var p = split[1].length;
                  if (p > precision) {
                    precision = p;
                  }
                  l += precision;
                }
              }
            } else {
              l = value.toString().length;
            }
            if (0 == l) {
              nullable = true;
            } else if (l > len) {
              len = l;
            }
          }
          if (numeric) {
            if (-1 == precision) {
              types[i]
                ..type = 'int'
                ..length = len;
            } else {
              types[i]
                ..type = 'decimal'
                ..length = len
                ..precision = precision;
            }
          } else {
            types[i]
              ..type = 'varchar'
              ..length = len;
          }
          types[i]..nullable = nullable;
        }
        break;
      default:
        throw new UnimplementedError('Not supported yet.');
    }
    final name = basenameWithoutExtension(o);
    final buf =
        new StringBuffer('CREATE TABLE $name (');
    for (var type in types) {
      buf.write('\n${fields[types.indexOf(type)]} ${type.type}');
      if (-1 != type.length) {
        if (-1 == type.precision) {
          buf.write('(${type.length})');
        } else {
          buf.write('(${type.length}, ${type.precision})');
        }
      }
      if (!type.nullable) {
        buf.write(' NOT');
      }
      buf.write(' NULL');
      if (type != types.last) {
        buf.write(',');
      }
    }
    buf.write('\n);\n');
    for(var row in decoded.skip(1)) {
      buf.write('\nINSERT INTO $name VALUES(');
      for(int i = 0; i < fields.length; ++i) {
        final type = types[i];
        switch(type.type) {
          case 'int':
          case 'decimal':
            buf.write(row[i]);
            break;
          case 'varchar':
            buf.write("'${row[i]}'");
            break;
        }
        if(i != fields.length - 1) {
          buf.write(', ');
        }
      }
      buf.write(');');
    }
    new File(o).writeAsStringSync(buf.toString());
  }

  @override
  String get description =>
      'Converts file to another format. Format is detected using out file extension.';

  @override
  String get name => 'convert';
}

class RenameCommand extends Command {
  RenameCommand() {
    addIoOptions(argParser);
    addParsingOptions(argParser);
  }

  @override
  void run() {
    final i = argResults['in'];
    if (null == i) {
      throw new ArgumentError.notNull('in');
    }
    final o = argResults['out'];
    if (null == o) {
      throw new ArgumentError.notNull('out');
    }
    final e = argResults['end-of-line'];
    final f = argResults['field-delimiter'];
    final t = argResults['text-delimiter'];
    final csv = new File(i).readAsStringSync();
    final detector = new FirstOccurenceSettingsDetector(
        eols: e, fieldDelimiters: f, textDelimiters: t);
    final decoded = decodeToList(csv, detector: detector);
    final fields = decoded[0];
    for (var pair in argResults.rest) {
      final split = pair.split('=');
      if (split.length != 2) {
        throw new ArgumentError('Invalid format: $pair');
      }
      final oldName = split[0], newName = split[1];
      final pos = fields.indexOf(oldName);
      if (-1 == pos) {
        throw new ArgumentError('Field $oldName not found.');
      }
      fields[pos] = newName;
    }
    final encoder = new ListToCsvConverter(
        eol: e.first, fieldDelimiter: f.first, textDelimiter: t.first);
    new File(o).writeAsStringSync(encoder.convert(decoded));
  }

  @override
  String get name => 'ren';

  @override
  String get description =>
      'Renames a column. Expects a space separated list of <oldname>=<newname>.';
}

class RmColCommand extends Command {
  RmColCommand() {
    argParser
      ..addOption('col',
          abbr: 'c',
          allowMultiple: true,
          help: 'Target column name or number.',
          splitCommas: true);
    addIoOptions(argParser);
    addParsingOptions(argParser);
  }

  @override
  void run() {
    final c = argResults['col'];
    if (null == c) {
      throw new ArgumentError.notNull('col');
    }
    final i = argResults['in'];
    if (null == i) {
      throw new ArgumentError.notNull('in');
    }
    final o = argResults['out'];
    if (null == o) {
      throw new ArgumentError.notNull('out');
    }
    final e = argResults['end-of-line'];
    final f = argResults['field-delimiter'];
    final t = argResults['text-delimiter'];
    final csv = new File(i).readAsStringSync();
    final detector = new FirstOccurenceSettingsDetector(
        eols: e, fieldDelimiters: f, textDelimiters: t);
    final decoded = decodeToList(csv, detector: detector);
    final fields = decoded[0];
    final positions = <int>[];
    for (var pos in c) {
      final value = int.parse(pos, onError: (pos) {
        pos = fields.indexOf(c);
        if (-1 == pos) {
          throw new ArgumentError('Column $pos not found.');
        }
        positions.add(pos);
        return -1;
      });
      if (value is int && value >= 0) {
        if (value >= fields.length) {
          throw new IndexError(value, fields);
        }
        positions.add(value);
      } else {
        throw new ArgumentError(
            'Parameter col must be positive integer or string.');
      }
    }
    for (var row in decoded) {
      for (var pos in positions) {
        row.removeAt(pos);
      }
    }
    final encoder = new ListToCsvConverter(
        eol: e.first, fieldDelimiter: f.first, textDelimiter: t.first);
    new File(o).writeAsStringSync(encoder.convert(decoded));
  }

  @override
  String get name => 'rm';

  @override
  String get description => 'Removes a column.';
}
