/// Copyright (C) 2015  Andrea Cantafio
///
/// This program is free software; you can redistribute it and/or modify
/// it under the terms of the GNU General Public License as published by
/// the Free Software Foundation; either version 2 of the License, or
/// (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License along
/// with this program; if not, write to the Free Software Foundation, Inc.,
/// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
library csvi.src.base;

import 'package:args/args.dart';
import 'package:console/console.dart';
import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';

Color defaultBackgroundColor = Color.BLACK;

Color defaultTextColor = Color.WHITE;

/// Adds --in (-i), --out (-o) options to [parser].
/// Multiple values are not allowed.
void addIoOptions(ArgParser parser) {
  parser
    ..addOption('in',
        abbr: 'i',
        allowMultiple: false,
        defaultsTo: null,
        help: 'Input file path.',
        valueHelp: 'path')
    ..addOption('out',
        abbr: 'o',
        allowMultiple: false,
        defaultsTo: null,
        help: 'Output file path.',
        valueHelp: 'path');
}

/// Adds --end-of-line (-e), --field-delimiter (-f), --text-delimiter (-t)
/// options to [parser].
/// All options allow multiple values.
void addParsingOptions(ArgParser parser) {
  parser
    ..addOption('end-of-line',
        abbr: 'e',
        allowMultiple: true,
        defaultsTo: r'\r\n',
        help: 'End of line. Multiple values allowed.',
        splitCommas: true,
        valueHelp: 'delimiter')
    ..addOption('field-delimiter',
        abbr: 'f',
        allowMultiple: true,
        defaultsTo: ',',
        help: 'Field delimiter. Multiple values allowed.',
        splitCommas: false,
        valueHelp: 'delimiter')
    ..addOption('text-delimiter',
        abbr: 't',
        allowMultiple: true,
        defaultsTo: '"',
        help: 'Text Delimiter. Multiple values allowed.',
        splitCommas: true,
        valueHelp: 'delimiter');
}

List<List> decodeToList(String csv, {CsvSettingsDetector detector}) {
  final decoder = new CsvToListConverter(csvSettingsDetector: detector);
  return decoder.convert(csv, parseNumbers: false);
}

List<Map<String, dynamic>> decodeToMap(String csv,
    {CsvSettingsDetector detector}) {
  List<Map<String, dynamic>> values = <Map<String, dynamic>>[];
  final decoded = decodeToList(csv, detector: detector);
  final fields = decoded.first;
  final mapper = <int, String>{};
  for(int i = 0; i < fields.length; ++i) {
    mapper[i] = fields[i];
  }
  for(int i = 1; i < decoded.length; ++i) {
    var map = <String, dynamic>{};
    for(int j = 0; j < fields.length; ++j) {
      map[mapper[j]] = decoded[i][j];
    }
    values.add(map);
  }
  return values;
}

void print(Object o, {Color color}) {
  if (null != color) {
    Console.setTextColor(color.id);
  }
  Console.write('${o is String ? o : o.toString()}\n');
  Console.setTextColor(defaultTextColor.id);
}
